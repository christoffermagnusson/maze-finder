Recursive alghorithm which will solve any maze , solvable following setup rules. 

Sample maze can look like :

* Start  01100
* 	     	01001
* 	     	00010
* 	     	11000 Goal

Whereas outside the maze is considered an illegal action. 1's are considered walls and 0's are considered valid tiles to walk on.