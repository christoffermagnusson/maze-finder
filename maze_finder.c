#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>

typedef struct Position {
	int posY;
	int posX;
};


void printMazeNew();
void simplePrint();
void findPath(int posY,  int posX);
void findPathStruct(struct Position *current);
FILE* getFile(char *filename);
void measureMaze(FILE* file);
void buildMaze();
void populateMaze(FILE* file);
bool isStartPositionBlocked();
bool isEndPositionBlocked();
bool isMazeContainingInvalidCharacters();


int rowCount = 0;
int columnCount = 0;
static int solutionCount = 0;
char **maze;

const int START_POS_BLOCKED = 1;
const int END_POS_BLOCKED = 2;
const int INVALID_CHARACTERS = 3;

int main(int *argc, char **argv){


	printf("\nInput file to open. (.txt will be appended) ");
	char filename [100];
	scanf("%s",&filename);
	strcat(filename, ".txt");
	FILE* fileToMeasure = fopen(filename, "r");
	while(!fileToMeasure){
		printf("\nNo file present with name %s, try another file\n", filename);
		scanf("%s",&filename);
		strcat(filename, ".txt");
		fileToMeasure = fopen(filename, "r");
	}


	measureMaze(fileToMeasure);
	fclose(fileToMeasure);
	buildMaze();

	FILE *fileToPopulate = getFile(filename);
	populateMaze(fileToPopulate);
	fclose(fileToPopulate);

	/**
	* Checks that the maze is properly setup with no blocking start positions and end positions
	* Also checks for invalid characters that are not '0' or '1'
	*/
	if(isStartPositionBlocked()){
		simplePrint();
		printf("\nThe starting position is blocked. Please change your input file");
		return START_POS_BLOCKED;
	}
	if(isEndPositionBlocked()){
		simplePrint();
		printf("\nThe goal position is blocked. Please change your input file");
		return END_POS_BLOCKED;
	}
	if(isMazeContainingInvalidCharacters()){
		simplePrint();
		printf("\nThe maze is containing invalid characters. Allowed characters are : 0 and 1. \nPlease change your input file");
		return INVALID_CHARACTERS;
	}



	findPath(0,0);


	struct Position startPos;
	startPos.posY = 0;
	startPos.posX = 0;
	//findPathStruct(&startPos);
	return 0;
}

void findPath(int posY, int posX){
	printMazeNew();
	bool goalReached = (posY==(rowCount-1) && posX==(columnCount-1)) ? true : false;
	bool outOfBounds = (posY < 0 || posY > (rowCount-1) || posX < 0 || posX > (columnCount-1)) ? true : false;
	if(goalReached){
		solutionCount++;
		maze[posY][posX] = 'G';
		//simplePrint();
		maze[posY][posX] = '0';
		return;
	}
	if(outOfBounds){return;}
	bool visited = (maze[posY][posX]=='X') ? true : false;
    bool wall = (maze[posY][posX]=='1') ? true : false;
	if(visited){return;}
	if(wall){return;}

	maze[posY][posX] = 'X';

	findPath(posY,(posX+1));
	findPath(posY,(posX-1));
	findPath((posY+1),posX);
	findPath((posY-1),posX);

	maze[posY][posX] = '0';

}

void findPathStruct(struct Position *current){
	bool goalReached = (current->posY==(rowCount-1) && current->posX==(columnCount-1)) ? true : false;
	bool outOfBounds = (current->posY < 0 || current->posY > (rowCount-1) || current->posX < 0 || current->posX > (columnCount-1)) ? true : false;
	if(goalReached){
		solutionCount++;
		maze[current->posY][current->posX] = 'G';
		printMazeNew();
		maze[current->posY][current->posX] = '0';
		return;
	}
	if(outOfBounds){return;}
	bool visited = (maze[current->posY][current->posX]=='X') ? true : false;
    bool wall = (maze[current->posY][current->posX]=='1') ? true : false;
	if(visited){return;}
	if(wall){return;}


	maze[current->posY][current->posX] = 'X';

	// Step east
	current->posX += 1;
	findPathStruct(current);
	// Step west
	current->posX -= 1;
	findPathStruct(current);
	// Step south
	current->posY += 1;
	findPathStruct(current);
	// Step north
	current->posY -= 1;
	findPathStruct(current);

	maze[current->posY][current->posX] = '0';

}


void printMazeNew(){
	printf("%c[2J%c[;H", (char) 27, (char) 27);
	printf("\n\n|******************************************|\n");
	printf("Solution %d :\n\n",solutionCount);
	for(int i=0; i<rowCount; i++){
		printf("\r");
		printf("%2d: ",i);
		for(int j=0; j<columnCount; j++){
			printf("[%c] ",maze[i][j]);

		}
		printf("\n");
	}
	sleep(1);
}

void simplePrint(){
	printf("\n\n|******************************************|\n");
	printf("Solution %d :\n\n",solutionCount);
	for(int i=0; i<rowCount; i++){
		for(int j=0; j<columnCount; j++){
			printf("[%c] ",maze[i][j]);
		}
		printf("\n");
	}
}

FILE* getFile(char *filename){
	FILE *file = fopen(filename, "r");
	if(!file){
		perror(filename);
		return 0;
	}
	return file;
}

void measureMaze(FILE* file){
	int c;
	int totalChar = 0;
	if(file){
		while((c=getc(file)) != EOF){
			if(c=='\n'){
				rowCount++;
			}
			else{
				totalChar++;
			}
		}
		columnCount = (totalChar/rowCount);
	}
}

void buildMaze(){
	maze = malloc(rowCount * sizeof(char *));
	for(int i=0; i<rowCount; i++){
		maze[i] = malloc(columnCount * sizeof(char));
	}
}

void populateMaze(FILE* file){
	int row=0,column=0;
	int c;
	if(file){
		while((c=getc(file)) != EOF){
			if(c=='\n'){
				row++;
				column=0;
			}else if(c!='\n'){
				if(column!=columnCount){
					maze[row][column] = (char) c;
					column++;
				}
			}
		}
	}
}

bool isStartPositionBlocked(){
	return maze[0][0]=='1' ? true : false;
}

bool isEndPositionBlocked(){
	return maze[rowCount-1][columnCount-1]=='1' ? true : false;
}

bool isMazeContainingInvalidCharacters(){
	for(int i=0; i<rowCount; i++){
		for(int j=0; j<columnCount; j++){

			if((maze[i][j]!='0') && (maze[i][j]!='1')){
				printf("\nFound invalid character");
				printf("\n[%d][%d]=%c",i,j,maze[i][j]);
				return true;
			}
		}
	}
	return false;
}