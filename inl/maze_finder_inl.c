#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>


void printMazeNew();
void simplePrint();
void findPath(int posY,  int posX);
FILE* getFile(char *filename);
bool measureMaze(FILE* file);
void buildMaze();
void populateMaze(FILE* file);
bool isStartPositionBlocked();
bool isEndPositionBlocked();
bool isMazeContainingInvalidCharacters();


int rowCount = 0;
int columnCount = 0;
static int solutionCount = 0;
char **maze;

const int START_POS_BLOCKED = 1;
const int END_POS_BLOCKED = 2;
const int INVALID_CHARACTERS = 3;
const int COLUMNS_NOT_ALIGNED = 4;


int main(int *argc, char **argv){


	printf("\nInput file to open. (.txt will be appended) ");
	char filename [100];
	scanf("%s",&filename);
	strcat(filename, ".txt");
	FILE* fileToMeasure = fopen(filename, "r");
	while(!fileToMeasure){
		printf("\nNo file present with name %s, try another file\n", filename);
		scanf("%s",&filename);
		strcat(filename, ".txt");
		fileToMeasure = fopen(filename, "r");
	}


	if(!measureMaze(fileToMeasure)){
		//simplePrint();
		printf("\nColumns are not of the same size. Please trim the columns in the input file.");
		return COLUMNS_NOT_ALIGNED;
	}
	fclose(fileToMeasure);
	buildMaze();

	FILE *fileToPopulate = getFile(filename);
	populateMaze(fileToPopulate);
	fclose(fileToPopulate);

	/**
	* Checks that the maze is properly setup with no blocking start positions and end positions
	* Also checks for invalid characters that are not '0' or '1'
	*/
	if(isStartPositionBlocked()){
		simplePrint();
		printf("\nThe starting position is blocked. Please change your input file");
		return START_POS_BLOCKED;
	}
	if(isEndPositionBlocked()){
		simplePrint();
		printf("\nThe goal position is blocked. Please change your input file");
		return END_POS_BLOCKED;
	}
	if(isMazeContainingInvalidCharacters()){
		simplePrint();
		printf("\nThe maze is containing invalid characters. Allowed characters are : 0 and 1. \nPlease change your input file");
		return INVALID_CHARACTERS;
	}
	printf("\n\n");
	simplePrint();
	printf("\nMaze correctly formatted. Start finding pathways...");
	printf("\n+----------------------------------------------------+\n");



	findPath(0,0);
	return 0;
}

void findPath(int posY, int posX){
	bool goalReached = (posY==(rowCount-1) && posX==(columnCount-1)) ? true : false;
	bool outOfBounds = (posY < 0 || posY > (rowCount-1) || posX < 0 || posX > (columnCount-1)) ? true : false;
	if(goalReached){
		solutionCount++;
		maze[posY][posX] = 'G';
		simplePrint();
		maze[posY][posX] = '0';
		return;
	}
	if(outOfBounds){return;}
	bool visited = (maze[posY][posX]=='X') ? true : false;
    bool wall = (maze[posY][posX]=='1') ? true : false;
	if(visited){return;}
	if(wall){return;}

	maze[posY][posX] = 'X';

	findPath(posY,(posX+1));
	findPath(posY,(posX-1));
	findPath((posY+1),posX);
	findPath((posY-1),posX);

	maze[posY][posX] = '0';

}



// Print method that animates the progress of the path finding
void printMazeNew(){

	printf("%c[2J%c[;H", (char) 27, (char) 27);
	printf("\n\n|******************************************|\n");
	printf("Solution %d :\n\n",solutionCount);
	for(int i=0; i<rowCount; i++){
		printf("\r");
		printf("%2d: ",i);
		for(int j=0; j<columnCount; j++){
			printf("[%c] ",maze[i][j]);

		}
		printf("\n");
	}
	usleep(1000000);

}

// Simple printing method that only prints the result
void simplePrint(){
	if(solutionCount!=0){
	printf("\n\n|******************************************|\n");
	printf("Solution %d :\n\n",solutionCount);
	}
	for(int i=0; i<rowCount; i++){
		for(int j=0; j<columnCount; j++){
			printf("[%c] ",maze[i][j]);
		}
		printf("\n");
	}
}

// Opens up a file
FILE* getFile(char *filename){
	FILE *file = fopen(filename, "r");
	if(!file){
		perror(filename);
		return 0;
	}
	return file;
}

// Measure the maze's rows and columns
bool measureMaze(FILE* file){
	int c;
	int totalChar = 0;
	if(file){
		while((c=getc(file)) != EOF){
			if(c=='\n'){
				rowCount++;
			}
			else{
				totalChar++;
			}
		}
		columnCount = (totalChar/rowCount);
	}
	// Check that the columns are of similar size
	rewind(file);
	int *columnCounter = malloc(rowCount * sizeof(int));
	int index = 0;
	while((c=getc(file)) != EOF){
		if(c=='\n'){
			 index++;
			 printf("\nRow %d nr columns: %d",index,*columnCounter);
			 columnCounter++;
		}else{
			*columnCounter+=1;
		}
	}
	columnCounter--;
	for(int i=rowCount; i>1; i--){
		int compareRowOne = *columnCounter;
		columnCounter--;
		int compareRowTwo = *columnCounter;
		if(compareRowOne!=compareRowTwo){
			return false;
		}
	}
	printf("\nMaze is measured correctly and has %d rows with %d columns each", rowCount, columnCount);
	return true;






}

// Allocates memory for the maze
void buildMaze(){
	maze = malloc(rowCount * sizeof(char *));
	for(int i=0; i<rowCount; i++){
		maze[i] = malloc(columnCount * sizeof(char));
	}
}


// populates the maze from the input file.
void populateMaze(FILE* file){
	int row=0,column=0;
	int c;
	if(file){
		while((c=getc(file)) != EOF){
			if(c=='\n'){
				row++;
				column=0;
			}else if(c!='\n'){
				if(column!=columnCount){
					maze[row][column] = (char) c;
					column++;
				}
			}
		}
	}
	printf("\nMaze populated with walls and roads. Checking that start position and end position are not blocked.");
}

// Check if start position is blocked
bool isStartPositionBlocked(){
	return maze[0][0]=='1' ? true : false;
}

// Check if end position is blocked.
bool isEndPositionBlocked(){
	return maze[rowCount-1][columnCount-1]=='1' ? true : false;
}

// Check for invalid characters. Valid characters are '0' and '1'
bool isMazeContainingInvalidCharacters(){
	for(int i=0; i<rowCount; i++){
		for(int j=0; j<columnCount; j++){

			if((maze[i][j]!='0') && (maze[i][j]!='1')){
				printf("\nFound invalid character");
				printf("\n[%d][%d]=%c\n\n",i,j,maze[i][j]);
				return true;
			}
		}
	}
	return false;
}